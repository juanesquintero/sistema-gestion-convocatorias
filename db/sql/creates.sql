-- **********************************
-- *********** Database *************
-- **********************************
-- create database if not exists jos;
-- use jos;
-- **********************************
-- ************ Tables **************
-- **********************************

-- table inquilinos
CREATE TABLE inquilinos (
	codigo VARCHAR (25) NOT NULL,
	nombre VARCHAR (250) NOT NULL,
	contacto VARCHAR (250) NULL,
	url VARCHAR (250) NULL,
	estado VARCHAR (50) NOT NULL,
	tipo VARCHAR (50) NOT NULL,
	fecha TIMESTAMPTZ NOT NULL,
	CONSTRAINT  pk_Inquilino
	PRIMARY KEY( codigo )
);

-- table cargos
CREATE TABLE cargos (
	inquilino VARCHAR (25) NOT NULL,
	codigo VARCHAR (25) NOT NULL,
	nombre VARCHAR (250) NOT NULL,
	nombre_ingles VARCHAR (250) NULL,
	objetivo TEXT NULL,
	CONSTRAINT  pk_Cargo
	PRIMARY KEY( inquilino, codigo )
);

-- table detalles
CREATE TABLE detalles (
	inquilino VARCHAR (25) NOT NULL,
	cargo VARCHAR (25) NOT NULL,
	orden VARCHAR (25) NOT NULL,
	nombre VARCHAR (250) NULL,
	contenido TEXT NOT NULL,
	tipo VARCHAR (50) NULL,
	disposicion VARCHAR (50) NOT NULL,
	CONSTRAINT  pk_Detalle 
	PRIMARY KEY( inquilino, cargo, orden )
);

-- table convocatorias
CREATE TABLE convocatorias (
	inquilino VARCHAR (25) NOT NULL,
	cargo VARCHAR (25) NOT NULL,
	codigo SERIAL NOT NULL,
	inicio TIMESTAMPTZ NOT NULL,
	fin TIMESTAMPTZ NOT NULL,
	ciudad VARCHAR (50) NULL,
	nro_plazas INT NOT NULL,
	comentarios TEXT NULL,
	CONSTRAINT  pk_Convocatoria
	PRIMARY KEY( inquilino, codigo )
);

-- table candidatos
CREATE TABLE candidatos (
	correo VARCHAR (250) NOT NULL,
	nombre VARCHAR (250) NOT NULL,
	referencia VARCHAR (500) NULL,
	CONSTRAINT  pk_Candidato
	PRIMARY KEY( correo )
);

-- table postulaciones
CREATE TABLE postulaciones (
	inquilino VARCHAR (25) NOT NULL,
	convocatoria INT NOT NULL,
	cargo VARCHAR (25) NOT NULL,
	candidato VARCHAR (250) NOT NULL,
	fecha TIMESTAMPTZ NOT NULL,
	comentarios TEXT NULL,
	adherencia INT NULL,
	justificacion TEXT NULL,
	tipo_evaluacion VARCHAR (50) NOT NULL,
	CONSTRAINT  pk_Postulacion
	PRIMARY KEY( inquilino, convocatoria, cargo, candidato )
);

-- table perfiles
CREATE TABLE perfiles (
	inquilino VARCHAR (25) NOT NULL,
	convocatoria INT NOT NULL,
	cargo VARCHAR (25) NOT NULL,
	candidato VARCHAR (250) NOT NULL,
	rasgo VARCHAR (50) NOT NULL,
	calificacion INTEGER NOT NULL,
	justificacion TEXT NULL,
	CONSTRAINT  pk_Perfil
	PRIMARY KEY( inquilino, convocatoria, cargo, candidato, rasgo)
);

-- ****************************************
-- ************ Foreign Keys **************
-- ****************************************

-- For cargos(fk_Cargo_Inquilino) 
ALTER TABLE cargos ADD
	CONSTRAINT fk_Cargo_Inquilino
	FOREIGN KEY ( inquilino )
	REFERENCES inquilinos ( codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE;

-- For convocatorias(fk_Convocatoria_Cargo) 
ALTER TABLE convocatorias ADD
	CONSTRAINT fk_Convocatoria_Cargo
	FOREIGN KEY ( inquilino, cargo )
	REFERENCES cargos ( inquilino, codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE;

-- For detalles(fk_Detalle_Cargo) 
ALTER TABLE detalles ADD
	CONSTRAINT fk_Detalle_Cargo
	FOREIGN KEY ( inquilino, cargo )
	REFERENCES cargos ( inquilino, codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE;

-- For postulaciones(fk_Postulacion_Convocatoria) 
ALTER TABLE postulaciones ADD
	CONSTRAINT fk_Postulacion_Convocatoria
	FOREIGN KEY ( inquilino, convocatoria )
	REFERENCES convocatorias ( inquilino, codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE;
-- For postulaciones(fk_Postulacion_Candidato) 
ALTER TABLE postulaciones ADD
	CONSTRAINT fk_Postulacion_Candidato
	FOREIGN KEY ( candidato )
	REFERENCES candidatos ( correo )
	ON DELETE CASCADE
    ON UPDATE CASCADE;
-- For postulaciones(fk_Postulacion_Cargo) 
ALTER TABLE postulaciones ADD
	CONSTRAINT fk_Postulacion_Cargo
	FOREIGN KEY ( inquilino, cargo )
	REFERENCES cargos ( inquilino, codigo )
	ON DELETE CASCADE
    ON UPDATE CASCADE;

-- For perfiles(fk_Perfil_Postulacion) 
ALTER TABLE perfiles ADD
	CONSTRAINT fk_Perfil_Postulacion
	FOREIGN KEY ( inquilino, convocatoria, cargo, candidato )
	REFERENCES postulaciones ( inquilino, convocatoria, cargo, candidato )
	ON DELETE CASCADE
    ON UPDATE CASCADE;
