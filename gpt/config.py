import os
from logging import StreamHandler, getLogger, FileHandler, Formatter, Logger
from pydantic_settings import BaseSettings, SettingsConfigDict

'''LOGGING CONFIG'''
base_dir = os.path.dirname(os.path.abspath(__file__))

log_format = '%(asctime)s:%(levelname)s:%(name)s:%(message)s'
log_dir = base_dir + '/../logs'

def create_logger(level: str, log_to_file: bool = False):
    level_upper = level.upper()
    logger = getLogger(f'{level}_logger')
    logger.setLevel(level_upper)

    try:
        file_handler = FileHandler(f'{log_dir}/{level_upper}.log')
        file_handler.setFormatter(Formatter(log_format))
        logger.addHandler(file_handler)
    except Exception as e:
        stream_handler = StreamHandler()
        stream_handler.setFormatter(Formatter(log_format))
        logger.addHandler(stream_handler)
        print('ERROR on file logger creation', e, flush=True)

def get_logger(level: str):
    env = os.getenv('APP_ENV', 'local')
    create_logger(level, env == 'local')
    return getLogger(level+'_logger')
'''END'''


class Settings(BaseSettings):
    app_name: str = "JOS GPT"
    model_config = SettingsConfigDict(env_file=".env")

    app_env: str

    openai_api_key: str = ''
    openai_assistant_id: str = ''

    error_logger: Logger = get_logger('error')


app_description = '''
Asistente GPT del Sistema de Gestion de Convocatorias (SGC). 🚀
###
'''
APP_CONFIG = {
    "title": 'JOS GPT',
    "version": '0.0.1',
    "description": app_description,
    "contact": {
        'name': 'Juan Bernardo Quintero',
        'email': 'juanbdo.quintero@gmail.com',
    },
    "openapi_tags": [
        {
            'name': 'gpt',
            'description': 'Operaciones con el asistente GPT del JOS.',
        },
    ],
    "swagger_ui_parameters": {"syntaxHighlight": False, "displayRequestDuration": True},
}
