import openai
from time import sleep
from mangum import Mangum
from pydantic import BaseModel
from config import Settings, APP_CONFIG
from starlette.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from internal import AppHTTPException, catch_exceptions_middleware
from fastapi import Body, FastAPI, File, Request, UploadFile, HTTPException
from utils import extract_file_text, complete_json_string, cv_to_json_instructions, cv_to_json_prompt, get_json_format

app = FastAPI(**APP_CONFIG)
handler = Mangum(app)


# App origins access
origins = ['*']
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=origins,
    allow_headers=origins,
)

# Code Exception
app.middleware('http')(catch_exceptions_middleware)


@app.exception_handler(AppHTTPException)
async def app_exception_handler(
    request: Request,
    exc: AppHTTPException
):
    body = exc.__dict__.copy()
    return JSONResponse(
        status_code=body.pop('status_code'),
        content=body
    )


class ChatData(BaseModel):
    thread_id: str
    message: str | None


settings = Settings()

client = openai.OpenAI(api_key=settings.openai_api_key)
assistant = client.beta.assistants.retrieve(
    assistant_id=settings.openai_assistant_id
)


@app.get('/', tags=['index'])
async def index() -> dict:
    return {'api': 'JOS GPT Assistant'}


@app.get(path='/chat/start', tags=['chat'])
async def start_conversation() -> dict:
    thread = client.beta.threads.create()
    return {'thread_id': thread.id}


# upload to google drive
@app.post(path='/chat/message', tags=['chat'])
async def chat(data: ChatData):
    thread_id = data.thread_id
    prompt_msg = data.message if data.message else ''
    if not thread_id:
        return {'error': 'Missing thread_id'}

    # Añadiendo la respuesta del cliente al hilo de la conversación e iniciando el asistente
    # Add the user's message to the thread
    try:
        client.beta.threads.messages.create(
            thread_id=thread_id,
            role='user',
            content=prompt_msg,
        )
        # Run the Assistant
        run = client.beta.threads.runs.create(
            thread_id=thread_id,
            assistant_id=settings.openai_assistant_id
        )

    # Check if the Run requires action (function call)
        while True:
            run_status = client.beta.threads.runs.retrieve(
                thread_id=thread_id,
                run_id=run.id
            )

            print(f'Run status: \n{run_status.status}\n\n')

            if run_status.status == 'completed':
                # Retrieve and return the latest message from the assistant
                # Imprimiendo la respuesta
                messages = client.beta.threads.messages.list(
                    thread_id=thread_id)
                response = messages.data[0].content[0].text.value

                return {
                    'response': response,
                    'usage': dict(run_status.usage)
                }

            if run_status.status == 'failed':
                return {
                    'error': run_status.last_error.code,
                    'detail': run_status.last_error.message,
                }

            sleep(1)  # Wait for a second before checking again
    except Exception as e:
        error_body = e.__dict__.get('body')
        return {
            'error': error_body if error_body.get('message') else str(e),
            'detail': 'OpenAI API Error',
        }


@app.post('/file/upload', tags=['file'])
async def upload_file(
    message: str = Body(...),
    file: UploadFile = File(...),
    response_format: str = 'json'
):

    if not message or not file:
        raise HTTPException(
            status_code=400, detail='Missing message or file.'
        )

    # Create a file in the OpenAI client
    message_file = client.files.create(
        file=(file.filename, file.file),
        purpose='assistants'
    )

    thread = client.beta.threads.create(
        messages=[
            {
                'role': 'user',
                'content': message,
                'attachments': [
                    {
                        'file_id': message_file.id,
                        'tools': [
                            {'type': 'file_search'}
                        ]
                    }
                ],
            }
        ]
    )

    run = client.beta.threads.runs.create_and_poll(
        thread_id=thread.id,
        assistant_id=assistant.id
    )

    messages = list(
        client.beta.threads.messages.list(
            thread_id=thread.id, run_id=run.id
        )
    )

    if messages and messages[0] and messages[0].content:
        if messages[0].content[0] and messages[0].content[0].text:

            message_content = messages[0].content[0].text

            if response_format == 'json':
                ai_response = get_json_format(
                    message_content.value
                )
            else:
                ai_response = message_content.value

            response = ai_response if ai_response else message_content.value

            return {
                'response': response,
                'file': {
                    'id': message_file.id,
                    'name': message_file.filename,
                }
            }

    return {
        'error': 'OpenAI API Error',
        'detail': 'Model did not respond with a valid output.',
    }


@app.post('/file/process', tags=['file'])
async def process_file(file: UploadFile = File(...)):

    if file.content_type != 'application/pdf':
        return {'error': 'Unsupported file format. Please upload a PDF file.'}

    try:
        file_text = await extract_file_text(file)
        response = client.chat.completions.create(
            model='gpt-3.5-turbo',
            messages=[
                {'role': 'system', 'content': cv_to_json_instructions},
                {'role': 'user', 'content': f'{cv_to_json_prompt}\n\n{file_text}'}
            ],
            temperature=0.5,
            max_tokens=1024,
            top_p=1.0,
            frequency_penalty=0.0,
            presence_penalty=0.0
        )

        # Check if the model response is valid
        if response.choices and response.choices[0].message.content:
            model_response = response.choices[0].message.content
            detail, json_cv = complete_json_string(model_response)
            return {
                'detail': detail,
                'json_cv': json_cv,
                'model_response': model_response if not json_cv else '',
                'extracted_text': file_text if not json_cv else '',
            }

        else:
            # If model response is not valid or empty
            return {
                'extracted_text': file_text,
                'error': 'Model did not respond with a valid output.'
            }

    except Exception as e:
        return {'error': str(e)}
