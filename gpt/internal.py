from typing import Any
from functools import lru_cache
import re
import traceback
from fastapi import Request

from starlette.responses import JSONResponse

from config import Settings


@lru_cache()
def get_settings():
    return Settings()


def get_exception():
    raw_exp = traceback.format_exc()
    exception_lines = [e.strip() for e in reversed(raw_exp.splitlines()[-3:])]
    short_exp = ' '.join(exception_lines)
    clean_exp = re.sub('File(.+), in', 'in', short_exp)
    return clean_exp, short_exp, raw_exp


async def catch_exceptions_middleware(request: Request, call_next):
    _req = request.__dict__.get('scope')
    method, path = _req.get('method'), _req.get('path')
    try:
        return await call_next(request)
    except Exception:
        exp, exception, _exception = get_exception()
        error_msg = f'ERROR {method} {path} -> {exp}'
        if(get_settings().error_logger):
            get_settings().error_logger.error(f'{error_msg} ---> {_exception}')
        return JSONResponse({'detail': error_msg}, status_code=500)


class AppHTTPException(Exception):
    def __init__(
        self,
        status_code: int,
        detail: str,
        error: str | None = None,
        data: Any | None = None,
        count: int | None = None,
    ):
        self.status_code = status_code
        self.detail = detail
        self.data = data
        self.count = count
        self.error = error
        if count is None:
            del self.count
