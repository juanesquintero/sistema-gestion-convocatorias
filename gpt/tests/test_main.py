from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


# Test case for the index endpoint
def test_index_endpoint():
    # Send a GET request to the index endpoint
    response = client.get('/')
    # Assert that the response status code is 200 (OK)
    assert response.status_code == 200
    # Assert that the response body contains the expected data
    assert response.json() == {'api': 'JOS GPT Assistant'}
