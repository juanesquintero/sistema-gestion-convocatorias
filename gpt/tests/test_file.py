from fastapi.testclient import TestClient
from main import app

# import json
# import pytest
# from unittest.mock import patch

client = TestClient(app)


def test_get_file():
    response = client.get('/file')
    assert response.status_code == 200
