from fastapi.testclient import TestClient
from main import app


client = TestClient(app)


def test_start_conversation():
    response = client.get('/chat/start')
    assert response.status_code == 200
    thread_id = response.json().get("thread_id")
    assert thread_id.startswith("thread_")


# Test case for successful chat request
def test_chat_success():
    # Send a POST request to the chat endpoint
    response = client.post('/chat/message', json={
        "thread_id": "12345",
        "message": "Hello, how are you?"
    })
    # Assert that the response status code is 200 (OK)
    assert response.status_code == 200
    # Assert that the response body contains the expected data
    assert "response" in response.json()
    assert "usage" in response.json()

# Test case for chat request with missing thread_id


def test_chat_missing_thread_id():
    # Send a POST request to the chat endpoint without thread_id
    response = client.post('/chat/message', json={
        "message": "Hello, how are you?"
    })
    # Assert that the response status code is 200 (OK)
    assert response.status_code == 200
    # Assert that the response body contains the expected error message
    assert response.json() == {"error": "Falta thread_id"}

# Test case for failed chat request


def test_chat_failed():
    # Send a POST request to the chat endpoint with invalid thread_id
    response = client.post('/chat/message', json={
        "thread_id": "invalid_id",
        "message": "Hello, how are you?"
    })
    # Assert that the response status code is 200 (OK)
    assert response.status_code == 200
    # Assert that the response body contains the expected error message
    assert "error" in response.json()
    assert "detail" in response.json()
