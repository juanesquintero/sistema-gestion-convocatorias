import json
import re
import PyPDF2
# import fitz  # PyMuPDF


cv_to_json_prompt = "Parse the following CV information into structured JSON:"
cv_to_json_instructions = "You are a highly skilled assistant capable of understanding and structuring complex curriculum vitae text into structured json formats."


def get_json_format(text: str):
    if text.startswith('{') and text.endswith('}'):
        return json.loads(text)

    # Find the start of the JSON part
    start = text.find("```json")
    if start == -1:
        return text  # JSON marker not found

    # Move past the "```json" marker (7 characters long)
    start += 7

    # Find the end of the JSON part
    end = text.find("```", start)
    if end == -1:
        return text  # Ending marker not found

    # Extract the JSON part
    json_text = text[start:end].strip()

    try:
        # Parse and format the JSON
        parsed_json = json.loads(json_text)
        return parsed_json
    except json.JSONDecodeError:
        return text


async def extract_file_text(file):
    # Read PDF file content
    await file.seek(0)
    pdf_reader = PyPDF2.PdfReader(file.file)
    full_text = ""

    # Extract text from each page
    for page in pdf_reader.pages:
        full_text += page.extract_text()

    return full_text


def complete_json_string(json_str):
    # Trim everything before the first opening brace
    json_str = json_str.replace("\n", "").replace(
        "```json", "").replace("```", "").strip()
    # This regex looks empty values : and fill those with null
    json_str = re.sub(r'(?<=:)(?=[\s]*[,\]}])', ' null', json_str)
    # This regex looks for strings starting with a quote and ending with a non-quote followed by a comma, brace, or end of line
    # It ensures the string is properly closed with a quote
    json_str = re.sub(
        r'\"(.*?)(?<!\\)(\"(?=\s*[,\]}])|$)', r'"\1"', json_str)

    start_index = json_str.find('{')
    if start_index != -1:
        json_str = json_str[start_index:]
    else:
        return "Invalid input: No JSON object could be decoded", {}

    stack = []

    # Iterate over each character in the string
    for char in json_str:
        if char == '{':
            stack.append('}')
        elif char == '[':
            stack.append(']')
        elif char == '}':
            if stack and stack[-1] == '}':
                stack.pop()
        elif char == ']':
            if stack and stack[-1] == ']':
                stack.pop()

    # Append missing closing braces and brackets
    while stack:
        json_str += stack.pop()

    json_str = json_str.strip()

    # Try to parse the JSON to check if it is valid
    try:
        parsed_json = json.loads(json_str)
        return "The JSON is now valid and can be parsed.", parsed_json
    except json.JSONDecodeError as e:
        return "The JSON is still invalid: " + str(e) + ': ' + json_str, {}
