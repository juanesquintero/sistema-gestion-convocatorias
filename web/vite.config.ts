import { defineConfig, loadEnv } from 'vite';
import react from '@vitejs/plugin-react-swc';
import svgr from 'vite-plugin-svgr';
import { vi } from 'vitest';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  // Load environment variables based on the mode (development or production)
  const env = loadEnv(mode, process.cwd());

  return {
    plugins: [react(), svgr()],
    // Server configuration for development only
    ...(mode === 'development' && {
      server: {
        host: "0.0.0.0",
        port: 3000,
        watch: {
          usePolling: true,
        },
      },
      resolve: {
        alias: {
          '@': path.resolve(__dirname, './src'),
        },
      },
      build: {
        outDir: 'dist',
        minify: 'terser', // Use terser for minification
        sourcemap: mode === 'development', // Generate sourcemaps in development mode
      },
      test: {
        globals: true,
        environment: 'jsdom',
        setupFiles: './src/setupTests.ts',
      },
      envDir: './',
      css: {
        preprocessorOptions: {
          scss: {
            additionalData: `@import "src/styles/variables.scss";`,
            localsConvention: 'camelCase',
          },
        },
      },
    }
    )
  }
});
