import "./App.scss";
import Chat from "./app/components/Chat/Chat";
import { Header } from "./app/components/Header/Header";
import { useState } from "react";
import ChatLogo from "./app/components/Chat/Logo/ChatLogo";

const App = () => {
  const [isOpen, setIsOpen] = useState(false);
  const closeChat = () => {
    if (isOpen) {
      setIsOpen(false);
    }
  };
  const openChat = () => {
    if (!isOpen) {
      setIsOpen(true);
    }
  };

  return (
    <>
      {isOpen && (
        <main className="relative border rounded-md mr-[70px] min-w-[470px]">
          <Header toggleChat={closeChat}></Header>
          <Chat />
        </main>
      )}
      <ToggleChatBtn toggleChat={openChat} />
    </>
  );
};

const ToggleChatBtn = ({ toggleChat }: { toggleChat: () => void }) => {
  return (
    <>
      <button
        onClick={toggleChat}
        className="w-16 h-16 m-1 p-4 rounded-full rounded-ss-xl  bg-slate-950 dark:bg-gray-200 border-2"
      >
        <ChatLogo className="w-8 mr-5 fill-white dark:fill-black"/>
      </button>
    </>
  );
};

export default App;
