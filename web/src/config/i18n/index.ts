import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import EN from './locales/en/translation.json';
import ES from './locales/es/translation.json';
import PT from './locales/pt/translation.json';

// the translations
const resources = {
	en: {
		translation: EN
	},
	es: {
		translation: ES
	},
	pt: {
		translation: PT
	}
};

i18n
	.use(Backend)
	.use(LanguageDetector)
	.use(initReactI18next)
	.init({
		resources,
		fallbackLng: 'es',
		interpolation: {
			escapeValue: false,
		},
	});
