interface TranslationNode {
	[key: string]: string | TranslationNode;
}

interface ITranslationLang {
	translation: TranslationNode;
}


export default ITranslationLang;
