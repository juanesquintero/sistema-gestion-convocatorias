const env = {
	apiBaseURL: import.meta.env.VITE_API_BASE_URL ?? 'http://localhost:9000',
};

export default env;
