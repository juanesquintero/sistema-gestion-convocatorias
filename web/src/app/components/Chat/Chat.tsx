import React, { useOptimistic, useState } from "react";
import { HTTPRespose, IMessage, MsgContent } from "../../shared/types";
import { Message } from "./Message/Message";
import { fecthData, postData } from "../../shared/utils";
import InputBox from "./InputBox/InputBox";
import StateBlock from "./StateBlock/StateBlock";
import { useTranslation } from "react-i18next";

const Chat: React.FC = () => {
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [input, setInput] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [thread, setThread] = useState<string | null>(
    localStorage.getItem("thread") ?? null
  );
  const [file, setFile] = useState<File | null>(null);
  const [errorMsg, setErrorMsg] = useState<string | null>(null);

  const { t } = useTranslation();

  const [optimisticMessages, addOptimisticMessage] = useOptimistic(
    messages,
    (state, newMessage: IMessage) => [...state, newMessage]
  );

  const setThreadID = (threadID?: string | null) => {
    if (threadID) {
      setThread(threadID);
      localStorage.setItem("thread", threadID);
    }
  };

  const sendChat = async (msg: MsgContent) => {
    setLoading(true);

    // const newMessages: IMessage[] = [];

    const userMsg: IMessage = {
      id: Date.now(),
      time: new Date(),
      text: msg.label,
      sender: "user",
    };

    // newMessages.push(userMsg);

    // Optimistically add the user's message
    addOptimisticMessage(userMsg);

    if (messages?.length === 0) {
      const response = await fecthData({ endpoint: "/chat/start" });
      if (checkError(response, userMsg.id)) return;
      const threadId = response?.data?.["thread_id"];
      if (threadId) {
        setThreadID(threadId as string);
      }
    }

    if (thread) {
      const response = await postData({
        endpoint: "/chat/message",
        body: {
          thread_id: thread,
          message: msg.content,
        },
      });
      if (checkError(response)) return;
      const botMsg: IMessage = {
        id: Date.now(),
        text: response?.response?.trim(),
        sender: "bot",
        time: new Date(),
      };
      // Optimistically add the bots message
      addOptimisticMessage(botMsg);
    } else {
      setErrorMsg(t("chat.startChatError"));
    }

    // setMessages([...messages, ...optimisticMessages]);
    setLoading(false);
  };

  const sendFile = async () => {
    if (!file) return;
    setFile(null);
    const formData = new FormData();
    formData.append("file", file as File);
    const msgContent: MsgContent = {
      label: file?.name || "File Name",
      content: formData || "",
    };
    sendChat(msgContent);
  };

  const sendMessage = async (e: React.FormEvent) => {
    e.preventDefault();
    if (file) return sendFile();
    const msgText = input.trim();
    if (!msgText) return;
    setInput("");
    setErrorMsg(null);
    const msgContent: MsgContent = {
      label: msgText,
      content: msgText,
    };
    sendChat(msgContent);
  };

  const checkError = (response: HTTPRespose, optimisticMsgId?: number) => {
    if (response?.error || !response?.status?.startsWith("2")) {
      setErrorMsg(response.error ?? "Error");
      setLoading(false);
      // Rollback: Remove the last optimistic message if it failed
      if (optimisticMsgId) {
        const index = optimisticMessages.findIndex(
          (msg: IMessage) => msg.id === optimisticMsgId
        );
        setMessages(optimisticMessages.splice(index, 1));
      }
      return true;
    }
    setErrorMsg(null);
    return false;
  };

  return (
    <div className="p-4 w-full max-w-screen-md min-h-[550px] flex flex-col">
      <ul className="">
        {optimisticMessages.map((msg: IMessage) => (
          <Message key={msg?.id} {...msg}></Message>
        ))}
      </ul>

      <StateBlock errorMsg={errorMsg} loading={loading} />

      <InputBox
        loading={loading}
        file={file}
        input={input}
        setInput={setInput}
        setFile={setFile}
        sendMessage={sendMessage}
      />
    </div>
  );
};

export default Chat;
