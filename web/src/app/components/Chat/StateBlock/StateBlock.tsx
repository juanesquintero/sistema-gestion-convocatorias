import { ErrorBanner, Spinner } from "../../../core";
import { MsgSkeleton } from "../../../core/MsgSkeleton/MsgSkeleton";

type StateBlockProps = {
  errorMsg?: string | null;
  loading?: boolean;
};

const StateBlock = ({ errorMsg, loading }: StateBlockProps) => {
  return (
    <div>
      {errorMsg && <ErrorBanner message={errorMsg} />}
      <span className="flex justify-center">
        {loading && <Spinner size="m" />}
      </span>
      {loading && <MsgSkeleton />}
    </div>
  );
};

export default StateBlock;
