import React from "react";
import { DocumentIcon, PaperClipIcon } from "@heroicons/react/24/solid";
import { XCircleIcon } from "@heroicons/react/24/outline";
import { FILE_FORMATS } from "../../../shared/constants";

type FileInputProps = {
  attachedFile: File | null;
  setAttachedFile: (file: File | null) => void;
  loading?: boolean;
};

const FileInput: React.FC<FileInputProps> = ({
  attachedFile,
  setAttachedFile,
  loading,
}: FileInputProps) => {
  const attachFile = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file?.type && FILE_FORMATS.includes(file.type)) {
      setAttachedFile(file);
    }
  };
  return (
    <>
      <label className="p-2 rounded-md border-2 border-inherit hover:bg-gray-200 cursor-pointer">
        <PaperClipIcon className="h-6 w-6 text-gray-500" />
        <input
          type="file"
          className="hidden"
          disabled={loading}
          accept={FILE_FORMATS.join(",")}
          onChange={attachFile}
        />
      </label>
      {attachedFile && (
        <div className="flex items-center justify-center px-2 rounded-lg shadow ml-1">
          <DocumentIcon
            className={`h-6 w-6 ${
              attachedFile?.type.includes("pdf")
                ? "text-red-500"
                : "text-yellow-500"
            }`}
          />
          <span className="ml-1 text-sm">
            <p className="font-medium truncate w-24" title={attachedFile?.name}>
              {attachedFile?.name}
            </p>

            <p className="text-gray-500">
              {attachedFile?.type.includes("pdf") ? "PDF" : "JSON"}{" "}
            </p>
          </span>
          <button
            type="button"
            onClick={() => {
              setAttachedFile(null);
            }}
            className="text-gray-700 hover:text-gray-900"
          >
            <XCircleIcon className="h-4 w-4" />
          </button>
        </div>
      )}
    </>
  );
};

export default FileInput;
