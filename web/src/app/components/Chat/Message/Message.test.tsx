import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Message } from "./Message";
import BotIcon from "../../../assets/icons/bot.svg";
import { IMessage } from "../../../shared/types";

describe("Message Component", () => {
  const userMessage: IMessage = {
    id: 1,
    sender: "user",
    time: new Date("2024-01-01T12:00:00"),
    text: "Hello from user",
  };

  const botMessage: IMessage = {
    id: 2,
    sender: "bot",
    time: new Date("2024-01-01T12:05:00"),
    text: "Hello from bot",
  };

  test("renders user message correctly", () => {
    render(<Message {...userMessage} />);
    expect(screen.getByText("user")).toBeInTheDocument();
    expect(screen.getByText("Hello from user")).toBeInTheDocument();
    expect(screen.getByRole("img", { name: "User Icon" })).toBeInTheDocument();
    expect(screen.getByText("12:00 PM")).toBeInTheDocument();
  });

  test("renders bot message correctly", () => {
    render(<Message {...botMessage} />);
    expect(screen.getByText("bot")).toBeInTheDocument();
    expect(screen.getByText("Hello from bot")).toBeInTheDocument();
    expect(screen.getByRole("img", { name: "Bot Icon" })).toHaveAttribute(
      "src",
      BotIcon
    );
    expect(screen.getByText("12:05 PM")).toBeInTheDocument();
  });
});
