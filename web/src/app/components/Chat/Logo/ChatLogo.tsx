import chatLogo from "../../../../../public/chat.svg";
import { ReactSVG } from "react-svg";

const ChatLogo = ({ className }: { className: string }) => (
  <ReactSVG
    src={chatLogo}
    className={`fill-current ${className}`}
  />
);

export default ChatLogo;
