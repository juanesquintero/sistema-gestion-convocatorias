import Filenput from "../FileInput/FileInput";
import { ArrowUpIcon } from "@heroicons/react/24/solid";
import { useTranslation } from "react-i18next";
import { Spinner } from "../../../core";

interface InputBoxProps {
  loading: boolean;
  file: File | null;
  input: string;
  setInput: React.Dispatch<React.SetStateAction<string>>;
  setFile: React.Dispatch<React.SetStateAction<File | null>>;
  sendMessage: (e: React.FormEvent) => Promise<void>;
}

const InputBox: React.FC<InputBoxProps> = ({
  loading,
  file,
  input,
  setInput,
  setFile,
  sendMessage,
}) => {
  const { t } = useTranslation();

  return (
    <form onSubmit={sendMessage} className="flex mt-auto">
      <Filenput
        attachedFile={file}
        setAttachedFile={setFile}
        loading={loading}
      />

      <input
        type="text"
        disabled={loading}
        value={input}
        onChange={(e) => setInput(e.target.value)}
        placeholder={t("chat.messagePlaceholder")}
        className="flex-1 p-2 mx-2 border-2 rounded-lg border-gray-300"
      />

      <button
        type="submit"
        disabled={loading}
        className="px-2 py-1 rounded-md border-2 border-inherit hover:bg-gray-200"
      >
        {loading ? (
          <Spinner size="s" />
        ) : (
          <ArrowUpIcon className="h-6 w-6 text-black" />
        )}
      </button>
    </form>
  );
};

export default InputBox;
