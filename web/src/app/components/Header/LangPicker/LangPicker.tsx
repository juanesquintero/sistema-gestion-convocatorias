import { useState } from "react";
import { ChevronDownIcon, ChevronUpIcon } from "@heroicons/react/24/solid";
import { useTranslation } from "react-i18next";
import { Lang } from "../Header";

type OptionProps = { lang: Lang; compressed?: boolean };
const Option = ({ lang, compressed }: OptionProps) => {
  const { t } = useTranslation();
  return (
    <div className="flex items-center px-2 py-1 w-100">
      <img
        src={lang.icon}
        alt={lang.value}
        className="h-4 w-4 mr-2 cursor-pointer"
      />
      <span className="text-sm">
        {compressed ? lang.value : t(`header.languages.${lang.label}`)}
      </span>
    </div>
  );
};

type LangPickerProps = {
  options: Lang[];
  compressed?: boolean;
};

export const LangPicker = ({ options }: LangPickerProps) => {
  const { i18n } = useTranslation();

  const [isOpen, setIsOpen] = useState(false);
  const [selectedLang, setSelectedLang] = useState(
    options.find((l) => l.value === i18n.language)
  );

  const changeLanguage = (lang: Lang) => {
    setSelectedLang(lang);
    i18n.changeLanguage(lang.value);
    setIsOpen(false);
  };
  return (
    <div className="relative inline-block p-0 m-0">
      <button
        className="flex items-center p-0 m-0 border rounded-md shadow-sm"
        onClick={() => setIsOpen(!isOpen)}
      >
        <Option lang={selectedLang ?? options[1]} compressed />
        {isOpen ? (
          <ChevronUpIcon className="ml-4 mr-2" />
        ) : (
          <ChevronDownIcon className="ml-4 mr-2" />
        )}
      </button>
      {isOpen && (
        <ul className="absolute mt-1 border rounded-md shadow-md backdrop-blur-md w-max">
          {options.map((opt) => (
            <li
              key={opt.value}
              className="flex items-center p-1 cursor-pointer "
              onClick={() => changeLanguage(opt)}
            >
              <Option lang={opt} />
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};
