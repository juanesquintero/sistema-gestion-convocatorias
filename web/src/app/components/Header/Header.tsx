import { useTranslation } from "react-i18next";
import { LangPicker } from "./LangPicker/LangPicker";
import { XCircleIcon } from "@heroicons/react/24/solid";
import { langsOptions } from "../../shared/constants";
import ChatLogo from "../Chat/Logo/ChatLogo";

export type Lang = (typeof langsOptions)[0];

export const Header = ({ toggleChat }: { toggleChat: () => void }) => {
  const { t } = useTranslation();

  return (
    <header className="border-b px-3 py-3 m-0">
      <div className="flex items-center justify-between w-full">
        <div className="flex">
          <ChatLogo className="w-10 h-10 mr-3 ml-1 fill-black dark:fill-white" />
          <span className="text-left">
            <p className="text-xl font-extrabold">{t("header.title")}</p>
            <p className="text-xs">{t("header.appName")}</p>
          </span>
        </div>
        <span className="justify-self-end">
          <LangPicker options={langsOptions} />
          <button
            onClick={toggleChat}
            className="absolute top-[-12px] right-[-12px] text-xl font-bold rounded-full backdrop-blur-3xl p-[4px]"
          >
            <XCircleIcon className="w-5 h-5" />
          </button>
        </span>
      </div>
    </header>
  );
};
