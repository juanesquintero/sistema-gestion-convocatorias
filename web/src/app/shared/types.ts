export interface IMessage {
    id: number;
    text?: string;
    sender?: Sender;
    time?: Date;
}

export interface MsgContent {
    label: string;
    content: string | FormData;
}

export type Sender = "user" | "bot";

export interface IGetRequest {
    endpoint: string;
}

export interface IPostRequest extends IGetRequest {
    body: Record<string, string | number | boolean | Date | null | undefined> | unknown;
}

export interface HTTPRespose<IData = Record<string, unknown>> {
    data?: IData | null;
    status?: string;
    error?: string | null;
}
