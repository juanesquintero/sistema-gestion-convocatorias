import { IPostRequest } from "./types";
import { postData } from "./utils";

const usePost = async ({ endpoint, body }: IPostRequest) => {
  return await postData({ endpoint, body });
};

export default usePost;
