import axios, { AxiosError } from "axios";
import env from "../../config/enviroment";
import { HTTPRespose, IGetRequest, IPostRequest } from "./types";

const baseReqConfig = {
    headers: {
        'Content-Type': 'application/json',
        timeout: 10000,
    },
}

export const postData = async ({ endpoint, body }: IPostRequest) => {
    try {
        const { data, status } = await axios.post(
            env.apiBaseURL + endpoint,
            body,
            baseReqConfig,
        );
        return { ...data, status };
    } catch (err) {
        const errMsg = 'HTTP POST Error'
        console.error(errMsg, err);
        const error: string = (err as AxiosError)?.message ?? errMsg;
        return { error };
    }
}

export const fecthData = async ({ endpoint }: IGetRequest): Promise<HTTPRespose> => {
    try {
        const { data, status } = await axios.get(
            env.apiBaseURL + endpoint,
            baseReqConfig,
        );
        return { ...data, status };
    } catch (err) {
        const errMsg = 'HTTP GET Error';
        console.error(errMsg, err);
        const error: string = (err as AxiosError)?.message ?? errMsg;
        return { error };
    }
}