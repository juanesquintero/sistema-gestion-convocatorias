import colFlag from "../../assets/icons/flag-col.svg";
import usFlag from "../../assets/icons/flag-us.svg";
import ptFlag from "../../assets/icons/flag-br.svg";

export const langsOptions = [
    {
        label: "spanish",
        icon: colFlag,
        value: "es",
    },
    {
        label: "english",
        icon: usFlag,
        value: "en",
    },
    {
        label: "portuguese",
        icon: ptFlag,
        value: "pt",
    },
];

export const MsgBoxStyles = {
    bot: "ml-5 pl-4 pr-2",
    user: "mr-5 pr-4 pl-2",
}

export const MsgStyles = {
    bot: "rounded-e-xl rounded-es-xl text-left bg-blue-500",
    user: "rounded-s-xl rounded-ee-xl text-right bg-green-500 ml-auto",
}

export const UserStyles = {
    bot: "flex flex-row",
    user: "flex flex-row-reverse space-x-reverse",
}

export const FILE_FORMATS = ["application/pdf", "application/json"]