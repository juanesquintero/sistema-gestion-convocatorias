# Job Openings System (JOS)

### (Sistema de Gestion de Convocatorias)


#### Clone the mono repository:

```bash
$ git clone <repository-url>
$ cd <repository-directory>
```

#### Prerequisites

- Docker installed on your machine
- Docker Compose installed on your machine


#### Create a `.env` file in the root directory and add the following environment variables:

```env
POSTGRES_USER=<your_postgres_user>
POSTGRES_PASSWORD=<your_postgres_password>
POSTGRES_DB=<your_postgres_db>
```

#### Build and run the Docker containers:

```bash
$ docker-compose up --build
```


# API

(Running FastAPI with Docker and PostgreSQL)


4. The FastAPI application will be available at `http://localhost:8000`.

### Docker Compose Configuration

Ensure your `docker-compose.yml` file looks like this:

```yaml
version: '3.8'

services:
  db:
    image: postgres:13
    environment:
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
      POSTGRES_DB: ${POSTGRES_DB}
    ports:
      - "5432:5432"
    volumes:
      - postgres_data:/var/lib/postgresql/data

  web:
    build: .
    command: uvicorn app.main:app --host 0.0.0.0 --port 8000 --reload
    volumes:
      - .:/code
    ports:
      - "8000:8000"
    depends_on:
      - db

volumes:
  postgres_data:
```

### Stopping the Containers

To stop the running containers, use:

```bash
$ docker-compose down
```

<hr>

## AWS deployment

### Lambda function zip

Commands to generate

Move to transactional api folder

```bash
$ cd api
```

Install/Generate dependencies packagesb

```bash
$ python3.12 -m pip  install --platform manylinux2014_x86_64 -t lib -r requirements.txt --only-binary=:all: --upgrade
```

Compress dependencies packages form requirements.txt

```bash
$ (cd lib; zip ../lambda_function.zip -r .)
```

Compress the source code in /app forlder

```bash
$ (cd app; zip ../lambda_function.zip -u ./**/*.py);
```

### Documentation Videos

AWS Lambda function Tutorial:

https://youtu.be/7-CvGFJNE_o

AWS RDS database Tutorial:

https://youtu.be/I_fTQTsz2nQ

# WEB

## React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
export default {
  // other rules...
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
    project: ["./tsconfig.json", "./tsconfig.node.json"],
    tsconfigRootDir: __dirname,
  },
};
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list
