from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


def test_get_all_cargos():
    # Send a GET request to the /cargos endpoint
    response = client.get('/cargos')
    # Assert that the response status code is 200 (OK)
    assert response.status_code == 200
    # Assert that the response body contains the list of cargos
    assert "cargos" in response.json()


def test_create_new_cargo():
    # Send a POST request to the /cargos endpoint with payload
    response = client.post('/cargos', json={
        "nombre": "Software Engineer",
        "nombre_corto": "SE",
        "nombre_ingles": "Software Engineer",
        "objetivo": "Develop high-quality software solutions"
    })
    # Assert that the response status code is 201 (Created)
    assert response.status_code == 201
    # Assert that the response body contains the created cargo
    assert "nombre" in response.json()
    assert "nombre_corto" in response.json()
    assert "nombre_ingles" in response.json()
    assert "objetivo" in response.json()


def test_get_specific_cargo():
    # Send a GET request to the /cargos/{cargo_id} endpoint
    response = client.get('/cargos/2')
    # Assert that the response status code is 200 (OK)
    assert response.status_code == 200
    # Assert that the response body contains the specific cargo
    assert "nombre" in response.json()
    assert "nombre_corto" in response.json()
    assert "nombre_ingles" in response.json()
    assert "objetivo" in response.json()


def test_update_cargo_success():
    # Send a PUT request to the /cargos/{cargo_id} endpoint
    response = client.put('/cargos/1', json={
        "nombre": "Senior Software Engineer",
        "nombre_corto": "SSE",
        "nombre_ingles": "Senior Software Engineer",
        "objetivo": "Lead and mentor a team of software engineers"
    })
    # Assert that the response status code is 200 (OK)
    assert response.status_code == 200
    # Assert that the response body contains the updated cargo
    assert "nombre" in response.json()
    assert "nombre_corto" in response.json()
    assert "nombre_ingles" in response.json()
    assert "objetivo" in response.json()


def test_delete_cargo():
    # Send a DELETE request to the /cargos/{cargo_id} endpoint
    response = client.delete('/cargos/1')
    # Assert that the response status code is 204 (No Content)
    assert response.status_code == 204
