import os
import json
from pydantic_settings import BaseSettings, SettingsConfigDict
from logging import StreamHandler, getLogger, FileHandler, Formatter, Logger

'''LOGGING CONFIG'''
base_dir = os.path.dirname(os.path.abspath(__file__))

log_format = '%(asctime)s:%(levelname)s:%(name)s:%(message)s'
log_dir = base_dir + '/../logs'


def create_logger(level: str, log_to_file: bool = False):
    level_upper = level.upper()
    logger = getLogger(f'{level}_logger')
    logger.setLevel(level_upper)

    try:
        file_handler = FileHandler(f'{log_dir}/{level_upper}.log')
        file_handler.setFormatter(Formatter(log_format))
        logger.addHandler(file_handler)
    except Exception as e:
        stream_handler = StreamHandler()
        stream_handler.setFormatter(Formatter(log_format))
        logger.addHandler(stream_handler)
        print('ERROR on file logger creation', e, flush=True)


def get_logger(level: str):
    env = os.getenv('APP_ENV', 'local')
    create_logger(level, env == 'local')
    return getLogger(level+'_logger')


'''END'''

'''LANG TRANSLATION CONFIG'''
locales_path = os.path.join(os.path.dirname(__file__), "./locales")


def make_translate(lang='es'):

    lang_path = os.path.join(locales_path, lang.lower() + ".json")

    with open(lang_path, "r") as lang_file:
        lang_dict = json.load(lang_file)

        def translate(key):
            keys = key.split('.')
            value = lang_dict
            for k in keys:
                value = value.get(k, '')
                if value == '':
                    return ''
            return value

        return translate


'''END'''


class Settings(BaseSettings):
    app_name: str = "JOS API"

    model_config = SettingsConfigDict(env_file=".env")

    token_secret_key: str = 'token-secret'

    app_env: str
    gpt_service_url: str = 'http://gpt'
    message_prompt: str = "Podrías evaluar la hoja de vida adjunta para el cargo con nombre corto {cargo}.pdf para el candidato con correo {candidato} y responder solo con el formato JSON especificado... formateando el texto en una solo línea"

    db_host: str
    db_user: str
    db_pwd: str
    db_schema: str
    app_lang: str = 'es'

    error_logger: Logger = get_logger('error')
    translate: callable = make_translate(app_lang)

    smtp_server: str = "smtp.gmail.com"
    smtp_port: int = 465  # Use 587 for TLS
    smtp_password: str
    sender_email: str
    recipient_email: str
    email_body_content: str = 'Adjunto encontraras la hoja de vida del candidato. {link}'
    email_subject: str = '(CognoTalent) Hoja de vida de {candidato}'


app_description = f'''
{Settings().translate('app.title')}. API 🚀
###
'''
APP_CONFIG = dict(
    title=f'{Settings().translate('app.name')} API',
    version='0.0.1',
    description=app_description,
    contact={
        'name': 'Juan Bernardo Quintero',
        'email': 'juanbdo.quintero@gmail.com',
    },
    servers=[{'url': '/'}],
    openapi_tags=[
        # {
        #     'name': 'Cargos',
        #     'description': f'{translate('operations_with')} con Cargos.',
        # },
        # {
        #     'name': 'Perfiles',
        #     'description': f'{translate('operations_with')} con Perfiles.',
        # },
        # {
        #     'name': 'Candidatos',
        #     'description': f'{translate('operations_with')} con Candidatos.',
        # },
    ],
    swagger_ui_parameters={
        "syntaxHighlight": False,
        "displayRequestDuration": True
    },
)
