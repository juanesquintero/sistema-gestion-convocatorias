from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Text, TIMESTAMP
from sqlalchemy.orm import relationship
from internal.db import Base


class Inquilino(Base):
    __tablename__ = "inquilinos"
    codigo = Column(String(25), primary_key=True)
    nombre = Column(String(250), nullable=False)
    contacto = Column(String(250))
    url = Column(String(250))
    estado = Column(String(50), nullable=False)
    tipo = Column(String(50), nullable=False)
    fecha = Column(TIMESTAMP(timezone=True), nullable=False)


class Cargo(Base):
    __tablename__ = "cargos"
    inquilino = Column(
        String(25),
        ForeignKey('inquilinos.codigo'),
        primary_key=True,
    )
    codigo = Column(String(25), primary_key=True)
    nombre = Column(String(250), nullable=False)
    nombre_ingles = Column(String(250))
    objetivo = Column(Text)

    convocatorias = relationship(
        "Convocatoria", back_populates="cargo_relation"
    )
    postulaciones = relationship(
        "Postulacion", back_populates="cargo_relation"
    )


class Detalle(Base):
    __tablename__ = "detalles"
    inquilino = Column(
        String(25),
        ForeignKey('inquilinos.codigo'),
        primary_key=True,
        index=True
    )
    cargo = Column(String(25), primary_key=True)
    orden = Column(String(25), primary_key=True)
    nombre = Column(String(250))
    contenido = Column(Text, nullable=False)
    tipo = Column(String(50))
    disposicion = Column(String(50), nullable=False)


class Convocatoria(Base):
    __tablename__ = "convocatorias"

    inquilino = Column(
        String(25),
        ForeignKey('inquilinos.codigo'),
        primary_key=True,
        index=True
    )
    cargo = Column(
        String(25),
        ForeignKey('cargos.codigo'),
        primary_key=True,
        index=True
    )
    codigo = Column(
        Integer, nullable=False,
        primary_key=True,
        autoincrement=True
    )
    inicio = Column(DateTime, nullable=False)
    fin = Column(DateTime, nullable=False)
    nro_plazas = Column(Integer, nullable=False)
    comentarios = Column(Text, nullable=True)

    cargo_relation = relationship("Cargo", back_populates="convocatorias")


class Candidato(Base):
    __tablename__ = "candidatos"

    correo = Column(String(250), index=True, nullable=False, primary_key=True)
    nombre = Column(String(250), index=True, nullable=False)
    referencia = Column(String(500))


class Postulacion(Base):
    __tablename__ = "postulaciones"

    inquilino = Column(
        String(25),
        ForeignKey('inquilinos.codigo'),
        primary_key=True,
        index=True
    )
    convocatoria = Column(
        Integer, ForeignKey("convocatorias.codigo"),
        nullable=False, primary_key=True
    )
    cargo = Column(
        String(25), ForeignKey("cargos.codigo"),
        nullable=False, primary_key=True
    )
    candidato = Column(
        String(250), ForeignKey("candidatos.correo"),
        nullable=False, primary_key=True
    )
    fecha = Column(TIMESTAMP, nullable=False)
    comentarios = Column(Text, nullable=True)
    adherencia = Column(Integer)
    justificacion = Column(Text)
    tipo_evaluacion = Column(String(50), nullable=False)

    cargo_relation = relationship("Cargo", back_populates="postulaciones")


class Perfil(Base):
    __tablename__ = "perfiles"

    convocatoria = Column(Integer, nullable=False, primary_key=True)
    cargo = Column(
        Integer, ForeignKey("cargos.codigo"),
        index=True, nullable=False, primary_key=True
    )
    candidato = Column(
        String(250), ForeignKey("candidatos.correo"),
        index=True, nullable=False, primary_key=True
    )
    rasgo = Column(String(50), index=True, nullable=False, primary_key=True)
    calificacion = Column(Integer, nullable=False)
    justificacion = Column(Text)
