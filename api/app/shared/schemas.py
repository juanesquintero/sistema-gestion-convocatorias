from datetime import datetime
from fastapi import UploadFile
from pydantic import BaseModel, ConfigDict, computed_field
from typing import Generic, TypeVar


T = TypeVar('T', bound=BaseModel)


class APIResponse(BaseModel, Generic[T]):
    data: T | list[T] | None
    detail: str | None

    @computed_field
    @property
    def count(self) -> int:
        if self.data:
            if isinstance(self.data, list):
                return len(self.data)
            return 1
        return 0

    model_config = ConfigDict(arbitrary_types_allowed=True)


class Token(BaseModel):
    access_token: str
    token_type: str


class Inquilino(BaseModel):
    codigo: str
    nombre: str
    contacto: str | None
    url: str | None
    estado: str
    tipo: str
    fecha: str


class CargoUpdate(BaseModel):
    nombre: str | None
    nombre_ingles: str | None
    objetivo: str | None

    model_config = ConfigDict(from_attributes=True)


class Cargo(CargoUpdate):
    inquilino: str
    codigo: str


class Detalle(BaseModel):
    inquilino: str
    cargo: str
    orden: str
    nombre: str | None
    contenido: str
    tipo: str | None
    disposicion: str


class ConvocatoriaBase(BaseModel):
    cargo: str
    inicio: datetime
    fin: datetime
    nro_plazas: int
    comentarios: str | None


class Convocatoria(ConvocatoriaBase):
    inquilino: str
    codigo: int
    cargo_relation: Cargo
    model_config = ConfigDict(from_attributes=True)


class Candidato(BaseModel):
    correo: str
    nombre: str
    referencia: str | None


class PostulacionPartial(BaseModel):
    inquilino: str
    convocatoria: int
    cargo: str
    fecha: datetime
    comentarios: str | None
    model_config = ConfigDict(from_attributes=True)


class PostulacionBase(PostulacionPartial):
    candidato: str
    cargo_relation: Cargo | None


class Postulacion(PostulacionBase):
    adherencia: int | None
    justificacion: str | None
    tipo_evaluacion: str | None


class PostulacionConvocatoria(BaseModel):
    postulacion: PostulacionPartial
    candidato: Candidato


class Perfil(BaseModel):
    inquilino: str
    convocatoria: int
    cargo: int
    candidato: str
    rasgo: str
    calificacion: int
    justificacion: str | None
