import re
import traceback

from datetime import timedelta
from fastapi import BackgroundTasks, HTTPException, Request

from pydantic import ValidationError
from starlette.responses import JSONResponse
from internal.dependencies import get_settings
from internal.auth import access_security
from sqlalchemy.exc import IntegrityError, DataError, OperationalError, ProgrammingError


def get_exception():
    raw_exp = traceback.format_exc()
    exception_lines = [e.strip() for e in reversed(raw_exp.splitlines()[-3:])]
    short_exp = ' '.join(exception_lines)
    clean_exp = re.sub('File(.+), in', 'in', short_exp)
    return clean_exp, short_exp, raw_exp


def validate_api_key(api_key: str, settings, msg: str = ''):
    if api_key != settings.api_key:
        raise HTTPException(
            status_code=401,
            detail=f'No autorizado, {msg}',
            headers={'WWW-Authenticate': 'Bearer'}
        )


def create_token(data: dict, expire_time: dict):
    return access_security.create_access_token(
        subject=data,
        expires_delta=timedelta(**expire_time),
    )


async def catch_exceptions_middleware(request: Request, call_next):
    _req = dict(request)
    method, path = _req.get('method'), _req.get('path')
    try:
        return await call_next(request)
        # response.background = get_background_tasks()
    except IntegrityError as e:
        error_msg = f'Integrity Error: {e.orig}'
        error_msg = log_exception(method, path)
        return JSONResponse(
            {
                'detail': 'Integrity Error: Duplicate entry or constraint violation.',
                'error': error_msg
            },
            status_code=400
        )
    except DataError as e:
        error_msg = f'Data Error: {e.orig}'
        error_msg = log_exception(method, path)
        return JSONResponse(
            {
                'detail': 'Data Error: Invalid data input.',
                'error': error_msg
            },
            status_code=400
        )
    except OperationalError as e:
        error_msg = f'Operational Error: {e.orig}'
        error_msg = log_exception(method, path)
        return JSONResponse(
            {
                'detail': 'Operational Error: Database operation failed, please try again later.',
                'error': error_msg
            },
            status_code=500
        )
    except ProgrammingError as e:
        error_msg = f'Programming Error: {e.orig}'
        error_msg = log_exception(method, path)
        return JSONResponse(
            {
                'detail': 'Programming Error: Check your SQL syntax.',
                'error': error_msg
            },
            status_code=400
        )
    except ValidationError as e:
        error_msg = log_exception(method, path)
        return JSONResponse({'detail': 'Pydantic Validation Error', 'errors': e.errors()}, status_code=422)
    except Exception as e:
        error_msg = log_exception(method, path)
        return JSONResponse(
            {
                'detail': 'Server Internal Error (Exception)',
                'error': error_msg
            },
            status_code=500
        )


def log_exception(method, path):
    exp, exception, _exception = get_exception()
    error_msg = f'ERROR {method} {path} -> {exp}'

    if(get_settings().error_logger):
        get_settings().error_logger.error(f'{error_msg} ---> {_exception}')

    return error_msg


def get_background_tasks():
    background_tasks = BackgroundTasks()
    background_tasks.add_task()
    return background_tasks
