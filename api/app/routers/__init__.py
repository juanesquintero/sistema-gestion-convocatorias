from . import cargos, candidatos, perfiles, convocatorias, postulaciones

app_routers = [
    cargos.router,
    candidatos.router,
    perfiles.router,
    convocatorias.router,
    postulaciones.router,
]
