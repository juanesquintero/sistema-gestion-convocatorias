
from copy import copy
from fastapi import APIRouter, BackgroundTasks, File, UploadFile

from internal.email import send_email_with_attachment
from internal.http import AppHTTPException
from internal.dependencies import AppSettings, DBSession, get_settings

from services import postulacion as service
from services.candidato import create_update_candidato
from shared.schemas import APIResponse, Postulacion, PostulacionBase, PostulacionConvocatoria

t = get_settings().translate

router = APIRouter(prefix='/postulaciones', tags=['Postulaciones'])

Response = APIResponse[
    Postulacion
    | PostulacionBase
]


@router.get('', response_model=Response, description=t('descriptions.announcements.fetch_all'))
@router.get('/', response_model=Response, description=t('descriptions.announcements.fetch_all'))
async def get_postulaciones(db: DBSession):
    return {
        'data': service.read_all(db),
        'detail': f'Postulaciones  {t('messages.founded')}',
    }


@router.get('/{convocatoria}/{candidato}',  response_model=Response, description=t('descriptions.announcements.fetch_one'))
async def get_postulacion(convocatoria: int, candidato: str, db: DBSession):
    return {
        'data': service.read_one(db, convocatoria, candidato),
        'detail': f'Postulacion  {t('messages.founded')}',
    }


@router.post('',  response_model=Response, description=t('descriptions.announcements.create_one'))
@router.post('/',  response_model=Response, description=t('descriptions.announcements.create_one'))
async def create_postulacion(postulacion: Postulacion, db: DBSession):
    postulacion = postulacion.model_dump()
    return {
        'data': service.create_one(db, postulacion),
        'detail': f'Postulacion {t('creadted')}',
    }


@router.post(
    '/convocatoria',
    response_model=Response,
    description=t('descriptions.announcements.apply')
)
async def apply_to_announcement(
    postulacion_convocatoria: PostulacionConvocatoria,
    db: DBSession,
    background_tasks: BackgroundTasks,
):
    postulacion_convocatoria = postulacion_convocatoria.model_dump()

    candidato = postulacion_convocatoria.get('candidato')
    postulacion = postulacion_convocatoria.get('postulacion')
    convocatoria = postulacion.get('convocatoria')

    # Already exist
    postulacion_exist = service.read_one(
        db,
        convocatoria,
        candidato.get('correo'),
        throw_excep=False
    )
    if postulacion_exist:
        raise AppHTTPException(
            status_code=400,
            detail=t('descriptions.announcements.already_existis'),
            error=t('descriptions.announcements.duplicated_key'),
        )

    candidato_db, op_type = await create_update_candidato(db, candidato)

    postulacion['candidato'] = candidato.get('correo')
    postulacion['tipo_evaluacion'] = 'manual'

    postulacion_db = service.create_one(db, postulacion)

    postulacion_response = copy(service.read_one(
        db,
        postulacion_db.get('convocatoria'),
        postulacion_db.get('candidato')
    ))

    return {
        'data': postulacion_response,
        'detail': f'Postulacion Convocatoria {t('messages.created')}',
    }


@router.post('/convocatoria/{convocatoria}/{candidato}', description=t('descriptions.announcements.upload_cv'))
async def upload_cv(
    convocatoria: int,
    candidato: str,
    db: DBSession,
    settings: AppSettings,
    file: UploadFile = File(...),
):
    # Already exists
    postulacion_db = service.read_one(
        db,
        convocatoria,
        candidato,
        throw_excep=False
    )
    if not postulacion_db:
        raise AppHTTPException(
            status_code=404,
            detail=f'Postulacion {t('messages.not_found')}',
            error=f'Postulacion {t('messages.not_found')}'
        )

    try:
        ai_response = await service.add_ai_evaluation(
            db=db,
            settings=settings,
            postulacion=postulacion_db,
            file=file,
        )
    except AppHTTPException as e:
        raise e
    except Exception as e:
        raise AppHTTPException(
            status_code=500,
            detail='Error en evaluación de AI',
            error=str(e),
            data=None
        )

    if ai_response.get('error'):
        raise AppHTTPException(
            status_code=500,
            detail='El asistente de IA no pudo procesar la hoja de vida',
            error=ai_response.get('error'),
            data={'prompt': ai_response.get('prompt')}
        )

    email_subject =  settings.email_subject.format(candidato=postulacion_db.candidato)
    result_link='https://cogno-talent.vercel.app/public/result/{id}'.format(id=postulacion_db.id)
    email_body =  settings.email_body_content.format(link=result_link)
    await send_email_with_attachment(email_subject, email_body, file, settings)

    return {
        'detail': t('descriptions.announcements.cv_uploaded'),
        'data': {
            'response': ai_response,
            'postulacion': postulacion_db
        },
    }


