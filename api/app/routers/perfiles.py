from fastapi import APIRouter
from internal.dependencies import DBSession
from shared import models, schemas


router = APIRouter(prefix='/perfiles', tags=['Perfiles'])


@router.get('')
@router.get('/')
async def get_perfiles(db: DBSession):
    return db.query(models.Perfil).all()


@router.post('')
@router.post('/')
async def post_perfil(perfil: schemas.Perfil, db: DBSession):
    db_perfil = models.Perfil(**perfil)
    db.add(db_perfil)
    db.commit()
    db.refresh(db_perfil)
    return db_perfil
