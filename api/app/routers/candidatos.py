from fastapi import APIRouter
from internal.dependencies import DBSession, get_settings
from shared.schemas import APIResponse, Candidato as CandidatoSchema
from services import candidato as service


router = APIRouter(prefix='/candidatos', tags=['Candidatos'])

Response = APIResponse[CandidatoSchema]

t = get_settings().translate


@router.get('', response_model=Response)
@router.get('/', response_model=Response)
async def get_candidatos(db: DBSession):
    return {
        'data': service.read_all(db),
        'detail': f'Candidatos  {t('messages.founded')}',
    }


@router.get('/{correo}', response_model=Response)
async def get_candidato(correo: str, db: DBSession):
    return {
        'data': service.read_one(db, correo),
        'detail': f'Candidato  {t('messages.founded')}',
    }


@router.post('', response_model=Response)
@router.post('/', response_model=Response)
async def create_candidato(candidato: CandidatoSchema, db: DBSession):
    candidato = candidato.model_dump()

    db_candidato, op_type = await service.create_update_candidato(db, candidato)

    detail_message = f'Candidato {t('messages.created')}',

    if op_type == 'updated':
        detail_message = f'Candidato {t('messages.updated')}',

    return {
        'data': db_candidato,
        'detail': detail_message,
    }


@router.delete('/{correo}', response_model=Response)
async def delete_candidato(correo: str, db: DBSession):
    return {
        'data': service.delete_one(db, correo),
        'detail': f'Candidato {t('messages.deleted')} {t('successfully')}'
    }
