from fastapi import APIRouter
from internal.dependencies import DBSession, get_settings
from shared.schemas import APIResponse, Cargo as CargoSchema, CargoUpdate
from services import cargo as service


router = APIRouter(prefix='/cargos', tags=['Cargos'])

Response = APIResponse[CargoSchema | CargoUpdate]

t = get_settings().translate


@router.get('', response_model=Response)
@router.get('/', response_model=Response)
async def get_cargos(db: DBSession):
    return {
        'data': service.read_all(db),
        'detail': f'Cargos  {t('messages.founded')}s',
    }


@router.get('/{cargo_id}', response_model=Response)
async def get_cargo(cargo_id: str, db: DBSession):
    return {
        'data': service.read_one(db, cargo_id),
        'detail': f'Cargo {t('messages.founded')}',
    }


@router.post('', response_model=Response, status_code=201)
@router.post('/', response_model=Response, status_code=201)
async def create_cargo(cargo: CargoSchema, db: DBSession):
    cargo = cargo.model_dump()
    return {
        'data': service.create_one(db, cargo),
        'detail': f'Cargo {t('messages.created')}',
    }


@router.put('/{cargo_id}', response_model=Response, status_code=201)
async def update_cargo(cargo_id: str, cargo: CargoUpdate, db: DBSession):
    cargo = cargo.model_dump()
    db_cargo = service.read_one(db, cargo_id)
    return {
        'data': service.update_one(db, cargo, db_cargo),
        'detail': f'Cargo {t('messages.updated')}',
    }


@router.delete('/{cargo_id}', response_model=Response,)
async def delete_cargo(cargo_id: str, db: DBSession):
    return {
        'data': service.delete_one(db, cargo_id),
        'detail': f'Cargo {t('messages.deleted')} {t('successfully')}'
    }
