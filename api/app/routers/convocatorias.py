from fastapi import APIRouter
from internal.dependencies import DBSession, get_settings
from shared.schemas import APIResponse, Convocatoria, ConvocatoriaBase
from services import convocatoria as service


router = APIRouter(prefix='/convocatorias', tags=['Convocatorias'])

Response = APIResponse[Convocatoria | ConvocatoriaBase]

t = get_settings().translate


@router.get('', response_model=Response)
@router.get('/', response_model=Response)
def get_convocatorias(db: DBSession):
    return {
        'data': service.read_all(db),
        'detail': f'Convocatorias  {t('messages.founded')}s',
    }


@router.get('/abiertas', response_model=Response)
def get_opened_convocatorias(db: DBSession):
    return {
        'data': service.read_opened(db),
        'detail': f'Convocatorias {t('messages.opened')} {t('messages.founded')}',
    }


@router.get('/{convocatoria_id}')
def get_convocatoria(convocatoria_id: int, db: DBSession):
    return {
        'data': service.read_one(db, convocatoria_id),
        'detail': f'Convocatoria  {t('messages.founded')}',
    }


@router.post('', response_model=Response, status_code=201)
@router.post('/', response_model=Response, status_code=201)
def create_convocatoria(convocatoria: ConvocatoriaBase, db: DBSession):
    convocatoria = convocatoria.model_dump()
    return {
        'data': service.create_one(db, convocatoria),
        'detail': f'Convocatoria {t('messages.created')}',
    }


@router.put('/{convocatoria_id}', response_model=Response, status_code=201)
def update_convocatoria(convocatoria_id: int, convocatoria: ConvocatoriaBase, db: DBSession):
    convocatoria = convocatoria.model_dump()
    db_convocatoria = service.read_one(db, convocatoria_id)
    return {
        'data': service.update_one(db, convocatoria, db_convocatoria),
        'detail': f'Convocatoria {t('messages.updated')}',
    }


@router.delete('/{convocatoria_id}', response_model=Response,)
def delete_convocatoria(convocatoria_id: int, db: DBSession):
    return {
        'data': service.delete_one(db, convocatoria_id),
        'detail': f'Convocatoria {t('messages.deleted')} {t('successfully')}',
    }
