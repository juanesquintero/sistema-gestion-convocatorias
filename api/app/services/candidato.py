from internal.dependencies import DBSession, get_settings
from internal.http import AppHTTPException
from shared.models import Candidato


t = get_settings().translate


def read_all(db: DBSession):
    candidatos = db.query(Candidato).all()
    if not candidatos:
        raise AppHTTPException(
            status_code=404,
            detail=f"{t('messages.not_found')} Candidatos"
        )
    return candidatos


def read_one(db: DBSession, candidato_id,  throw_excep=True):
    candidato = db.query(Candidato).filter(
        Candidato.correo == candidato_id
    ).first()
    if candidato is None and throw_excep:
        raise AppHTTPException(
            status_code=404,
            detail=f"Candidato {t('messages.not_found')}"
        )
    return candidato


def create_one(db: DBSession, candidato):
    db_candidato = Candidato(**candidato)
    db.add(db_candidato)
    db.commit()
    db.refresh(db_candidato)
    return candidato


def update_one(db: DBSession, candidato, db_candidato):
    for field, value in candidato.items():
        setattr(db_candidato, field, value)
    db.commit()
    db.refresh(db_candidato)
    return db_candidato


def delete_one(db: DBSession, candidato_id):
    db_candidato = read_one(db, candidato_id)
    db.delete(db_candidato)
    db.commit()
    return db_candidato


async def create_update_candidato(db: DBSession, candidato: dict):
    correo = candidato.get('correo')

    candidato_exists = read_one(db, correo, throw_excep=False)

    # Check if candidato exists and just Patch nombre and referencia
    if candidato_exists:
        db_candidato = read_one(db, correo)
        return update_one(db, candidato, db_candidato), 'updated'

    # Either way create a new candidato
    return create_one(db, candidato), 'created'
