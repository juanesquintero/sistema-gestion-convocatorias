from internal.dependencies import get_settings, DBSession
from internal.http import AppHTTPException
from shared.models import Cargo


t = get_settings().translate


def read_all(db: DBSession):
    cargos = db.query(Cargo).all()
    if not cargos:
        raise AppHTTPException(
            status_code=404,
            detail=f"{t('messages.not_found')} Cargos",
        )
    return cargos


def read_one(db: DBSession, cargo_id,  throw_excep=True):
    cargo = db.query(Cargo).filter(Cargo.codigo == cargo_id).first()
    if cargo is None and throw_excep:
        raise AppHTTPException(
            status_code=404,
            detail=f"Cargo {t('messages.not_found')}",
        )
    return cargo


def create_one(db: DBSession, cargo):
    db_cargo = Cargo(**cargo)
    db.add(db_cargo)
    db.commit()
    db.refresh(db_cargo)
    return cargo


def update_one(db: DBSession, cargo, db_cargo):
    for field, value in cargo.items():
        setattr(db_cargo, field, value)
    db.commit()
    db.refresh(db_cargo)
    return db_cargo


def delete_one(db: DBSession, cargo_id):
    db_cargo = read_one(db, cargo_id)
    db.delete(db_cargo)
    db.commit()
    return db_cargo
