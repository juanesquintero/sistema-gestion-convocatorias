from sqlalchemy import asc, desc
from internal.dependencies import get_settings, DBSession
from internal.http import AppHTTPException
from shared.models import Convocatoria
from datetime import datetime

t = get_settings().translate


def read_all(db):
    convocatorias = db.query(Convocatoria).all()
    if not convocatorias:
        raise AppHTTPException(
            status_code=404,
            detail=f"{t('messages.not_found')} Convocatorias",
        )
    return convocatorias


def read_opened(db):
    convocatorias = db.query(Convocatoria).filter(
        Convocatoria.fin > datetime.now()
    ).order_by(desc(Convocatoria.inicio)).all()
    if not convocatorias:
        raise AppHTTPException(
            status_code=404,
            detail=f"{t('messages.not_found')} Convocatorias",
        )
    return convocatorias


def read_one(db, convocatoria_id,  throw_excep=True):
    convocatoria = db.query(Convocatoria).filter(
        Convocatoria.codigo == convocatoria_id).first()
    if convocatoria is None and throw_excep:
        raise AppHTTPException(
            status_code=404,
            detail=f"{t('messages.not_found')} Convocatoria"
        )
    return convocatoria


def create_one(db, convocatoria):
    db_convocatoria = Convocatoria(**convocatoria)
    db.add(db_convocatoria)
    db.commit()
    db.refresh(db_convocatoria)
    return convocatoria


def update_one(db, convocatoria, db_convocatoria):
    for field, value in convocatoria.items():
        setattr(db_convocatoria, field, value)
    db_convocatoria.codigo = None
    db.commit()
    db.refresh(db_convocatoria)
    return db_convocatoria


def delete_one(db, convocatoria_id):
    db_convocatoria = read_one(db, convocatoria_id)
    db.delete(db_convocatoria)
    db.commit()
    return db_convocatoria
