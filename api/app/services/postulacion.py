import json
import requests
from random import randint
from sqlalchemy.orm.strategy_options import joinedload
from shared.models import Postulacion
from internal.http import AppHTTPException
from .convocatoria import read_one as read_convocatoria
from internal.dependencies import DBSession, AppSettings, get_settings
from fastapi import UploadFile

t = get_settings().translate


def read_all(db: DBSession):
    postulaciones = db.query(Postulacion).options(
        joinedload(Postulacion.cargo_relation)).all()
    if not postulaciones:
        raise AppHTTPException(
            status_code=404, detail=f'{t('messages.not_found')} Postulaciones',
        )
    return postulaciones


def read_one(db: DBSession, convocatoria: int, candidato: str, throw_excep=True):
    convocatoria_db = read_convocatoria(db, convocatoria)
    postulacion = db.query(Postulacion).options(joinedload(Postulacion.cargo_relation)).filter(
        Postulacion.inquilino == convocatoria_db.inquilino,
        Postulacion.convocatoria == convocatoria_db.codigo,
        Postulacion.cargo == convocatoria_db.cargo,
        Postulacion.candidato == candidato,
    ).first()
    if postulacion is None and throw_excep:
        raise AppHTTPException(
            status_code=404, detail=F'Postulacion {t('messages.not_found')}'
        )
    return postulacion


def create_one(db: DBSession, postulacion):
    db_postulacion = Postulacion(**postulacion)
    db.add(db_postulacion)
    db.commit()
    db.refresh(db_postulacion)
    return postulacion


def update_one(db: DBSession, postulacion, db_postulacion):
    for field, value in postulacion.items():
        setattr(db_postulacion, field, value)
    db_postulacion.codigo = None
    db.commit()
    db.refresh(db_postulacion)
    return db_postulacion


def delete_one(db: DBSession, postulacion_id):
    db_postulacion = read_one(db, postulacion_id)
    db.delete(db_postulacion)
    db.commit()
    return db_postulacion


def add_mock_evaluation(db: DBSession, postulacion):
    db_postulacion = read_one(
        db,
        postulacion.get('convocatoria'),
        postulacion.get('candidato')
    )
    experience = randint(38, 83)
    capacity = randint(38, 83)
    knowledge = randint(38, 83)
    rates = [experience, capacity, knowledge]
    global_rate = sum(rates)/len(rates)

    MOCKED_EVALUATION = {
        'tipo_evaluacion': 'automatica',
        'adherencia': global_rate,
        'justificacion': str(json.dumps([
            {
                'criterio': 'Experiencia laboral',
                'puntaje': experience,
                'justificacion': 'Experiencia relevante en arquitectura de software, gestión de proyectos y tecnologías J2EE.'
            },
            {
                'criterio': 'Competencias',
                'puntaje': capacity,
                'justificacion': 'Demostrada habilidad para liderar equipos y trabajar en entornos multiculturales.'
            },
            {
                'criterio': 'Conocimientos',
                'puntaje': knowledge,
                'justificacion': 'Educación formal relevante y certificaciones específicas en tecnologías clave.'
            }
        ], indent=4)),
    }
    update_one(db, MOCKED_EVALUATION, db_postulacion)


async def add_ai_evaluation(db: DBSession, settings: AppSettings, postulacion, file: UploadFile):
    ai_prompt = settings.message_prompt.format(
        cargo=postulacion.cargo,
        candidato=postulacion.candidato
    )
    settings.error_logger.error(f'AI Prompt: {ai_prompt}')

    ai_api_response = requests.post(
        settings.gpt_service_url + '/file/upload',
        data={
            'message': ai_prompt,
        },
        files={
            'file': (file.filename, file.file, file.content_type)
        },
        headers={
            'accept': 'application/json',
        },
        params={
            'response_format': 'json',
        },
    )

    settings.error_logger.error(
        f'AI Response: {ai_api_response.url} -->{ai_api_response.text}'
    )

    response_body = ai_api_response.json()
    ai_response = response_body.get('response')

    try:
        if isinstance(ai_response, str):
            ai_evaluation = json.loads(
                ai_response
            )
        else:
            ai_evaluation = ai_response

        justificacion = json.dumps(
            ai_response.get('justificaciones'),
            indent=2
        )
        AI_EVALUATION = {
            'tipo_evaluacion': 'automatica',
            'adherencia': ai_evaluation.get('adherencia'),
            'justificacion': str(justificacion),
        }

        update_one(db, AI_EVALUATION, postulacion)

        return {
            **ai_evaluation,
            'prompt': ai_prompt,
        }

    except Exception as e:
        AI_EVALUATION = {
            'tipo_evaluacion': 'automatica',
            'adherencia': 0,
            'justificacion': str(ai_response),
        }

        update_one(db, AI_EVALUATION, postulacion)
        return {
            'response': ai_response,
            'response_body': response_body,
            'prompt': ai_prompt,
        }

        # raise AppHTTPException(
        #     status_code=500,
        #     detail='No se obtuvo una respuesta tipo json de la evaluación de AI',
        #     error='Respuesta Asistente: ' + str(response_body),
        #     data=None
        # )
