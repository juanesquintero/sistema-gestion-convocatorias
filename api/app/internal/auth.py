
from datetime import datetime, timedelta

from fastapi import Depends, HTTPException, Header

from internal.dependencies import get_settings
from config import Settings

import jwt
from fastapi_jwt import JwtAccessBearer


async def create_access_token(
    data: dict,
    expires_delta: timedelta | None = None,
    settings: Settings = Depends(get_settings)
):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.now(datetime.UTC) + expires_delta
    else:
        expire = datetime.now(datetime.UTC) + timedelta(minutes=15)
    to_encode.update({'exp': expire})
    encoded_jwt = jwt.encode(
        to_encode,
        settings.secret_key,
        algorithm='HS256'
    )

    return encoded_jwt


async def get_jwt(token: str = Header(...)):
    if token != 'fake-super-secret-token':
        raise HTTPException(status_code=400, detail='Token header invalid')


access_security = JwtAccessBearer(
    secret_key=get_settings().token_secret_key,
    auto_error=True
)
