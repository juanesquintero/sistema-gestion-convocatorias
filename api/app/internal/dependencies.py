
from typing import Annotated
from functools import lru_cache
from fastapi import Depends
from sqlalchemy.orm import Session
from config import Settings
from internal.db import SessionLocal


async def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@lru_cache()
def get_settings():
    return Settings()


DBSession = Annotated[Session, Depends(get_db)]

AppSettings = Annotated[Settings, Depends(get_settings)]
