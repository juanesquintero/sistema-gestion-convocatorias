import smtplib
from email.message import EmailMessage
from fastapi import UploadFile
from config import Settings


async def send_email_with_attachment(subject: str, file: UploadFile, settings: Settings):
    message = EmailMessage()
    message["From"] = settings.sender_email
    message["To"] = settings.recipient_email
    message["Subject"] = subject
    message.set_content(settings.email_body_content)

    await file.seek(0)
    file_content = await file.read()
    await file.seek(0)

    message.add_attachment(
        file_content,
        maintype='application',
        subtype='octet-stream',
        filename=file.filename
    )

    # Send the email
    with smtplib.SMTP_SSL(settings.smtp_server, settings.smtp_port) as server:
        server.login(settings.sender_email, settings.smtp_password)
        server.send_message(message)
