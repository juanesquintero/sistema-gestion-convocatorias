from mangum import Mangum
from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse

from .config import APP_CONFIG
from routers import app_routers
from shared.utils import catch_exceptions_middleware
from internal.http import AppHTTPException
from internal.dependencies import get_settings

app = FastAPI(**APP_CONFIG)

# App origins access
app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        'http://cogno-talent.vercel.app',
        'https://cogno-talent.vercel.app',
        'http://vercel.app',
        'https://vercel.app',
        '*',
    ],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"],
    allow_headers=['*'],
)


# Code Exception
app.middleware('http')(catch_exceptions_middleware)


@app.exception_handler(AppHTTPException)
async def app_exception_handler(
    request: Request,
    exc: AppHTTPException
):
    body = exc.__dict__.copy()
    return JSONResponse(
        status_code=body.pop('status_code'),
        content=body
    )


t = get_settings().translate


@app.get('/')
async def index(lang: str = 'en') -> dict:
    return {'api': t('app.api_name')}


@app.get('/health')
async def health_check() -> dict:
    return {'status': 'OK'}


for router in app_routers:
    app.include_router(router)


# Lambda handler
handler = Mangum(app)
