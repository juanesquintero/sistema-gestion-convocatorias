fastapi==0.115.8

pydantic==2.10.6
pydantic-settings==2.7.1
pydantic-core==2.27.2

psycopg2-binary==2.9.10
alembic==1.14.1
SQLAlchemy==2.0.37


PyJWT===2.10.1
fastapi-jwt===0.3.0
Authlib===1.4.1


requests===2.32.3

python-multipart==0.0.20
aiofiles==24.1.0

uvicorn==0.34.0
mangum==0.19.0
