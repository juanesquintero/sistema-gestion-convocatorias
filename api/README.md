# Crear un App Password para Gmail

Si tiene habilitada la verificación de doble factor 2FA en su cuenta de Google, debe crear una contraseña de aplicación App Password:

## Pasos para crear una contraseña de aplicaciones en Gmail

1. Ve a tu [Cuenta de Google](https://myaccount.google.com/).
2. Selecciona `Seguridad` en el menú de la izquierda.
3. En la sección `Acceso a Google`, selecciona `Contraseñas de aplicaciones`.
4. Es posible que necesites iniciar sesión nuevamente.
5. En el menú desplegable `Seleccionar la aplicación`, elige la aplicación que estás utilizando. Si no está en la lista, selecciona `Otra (nombre personalizado)` e ingresa el nombre de la aplicación.
6. En el menú desplegable `Seleccionar el dispositivo`, elige el dispositivo que estás utilizando. Si no está en la lista, selecciona `Otra (nombre personalizado)` e ingresa el nombre del dispositivo.
7. Selecciona `Generar`.
8. Sigue las instrucciones para ingresar la Contraseña de aplicaciones. La Contraseña de aplicaciones es el código de 16 caracteres que aparece en una barra amarilla en tu dispositivo.
9. Ingresa la Contraseña de aplicaciones en lugar de tu contraseña habitual en el campo `smtp_password` en tu código.
